// Libraries
import express from "express";
import passport from "passport";

//Database Model
import { ReviewModel } from "../../database/allModels";

//Validation
import{ValidateResId} from '../../validation/review';
import{ValidateId} from '../../validation/common';

const Router = express.Router();

/**
 * Route        /:resid
 * Des          Get all reviews for a paticular restaurant
 * params       resid
 * Access       Public 
 * Method       GET
 */
Router.get("/:resid", async (req,res) => {
    try{
        await ValidateResId(req.params);
        const {resid} = req.params;
        const reviews = await ReviewModel.find({ restaurant: resid});

        return res.json({ reviews });
    } catch(error) {
        return res.status(500).json({error: error.message});
    }
});

/**
 * Route        /new
 * Des          POST: adding new food/restaurantreview and rating
 * params       none
 * Access       Private
 * Method       POST
 */
Router.post("/new", passport.authenticate("jwt"), async (req,res) => {
try {
    const {_id} = req.session.passport.user._doc;
    const { reviewData } = req.body;

    await ReviewModel.create({ ...reviewData, user: _id });

    return res.json({ reviews: "Successfully Created a Review" });
} catch(error) {
    return res.status(500).json({error: error.message});
}
});

/**
 * Route        /delete/:id
 * Des          Delete a specific review
 * params       _id
 * Access       Public 
 * Method       DELETE
 */
Router.delete('/delete/:id', async (req,res) => {
    try{
        await ValidateId(req.params);
        const { _id } = req.params;

        await ReviewModel.findByIdAndDelete(_id);

        return res.json({ review: "Successfully deleted the review" });
    } catch(error) {
        return res.status(500).json({error: error.message});
    }
})

export default Router;