// Libraries
import  express from 'express';


//Database model
import { MenuModel, ImageModel } from '../../database/allModels';

// Validation
import {ValidateId} from '../../validation/common';

const Router = express.Router();

/**
 * Route        /List
 * Des          Get all list menu based on Restaurant id
 * params       _id
 * Access       Public 
 * Method       GET
 */

Router.get('/list/:_id', async (req,res) => {
    try {
        await ValidateId(req.params);
        const {_id} = req.params;
        const menus = await MenuModel.findById(_id);

        if(!menus) {
            res.status(404).json({error: "No menu present for this restaurant" });
        }

        return res.json({ menus });
    } catch(error){
        return res.status(500).json({ error: error.message });
    }
});


/**
 * Route        /image
 * Des          Get all list menu images with restaurant id
 * params       _id
 * Access       Public 
 * Method       GET
 */
Router.get('/image:_id', async (req,res) => {
    try {
        await ValidateId(req.params);
        const {_id} = req.params;
        const menuImages = await ImageModel.findOne(_id);

        //TODO: validate if the images are present or not, throw error if not present
        if(!menuImages) {
            res.status(404).json({error: "No menuImages present for this restaurant" });
        }

        return res.json({ menuImages });
    } catch (error) {
        return res.status(500).json({ error: error.message });   
    }
});

export default Router;