import joi from 'joi';


export const ValidateResId = (resid) => {
    const Schema = joi.object({
        resid: joi.string().required(),

    });

    return Schema.validateAsync(resid);
};