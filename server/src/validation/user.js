import joi from 'joi';


export const ValidateUserId = (userid) => {
    const Schema = joi.object({
        userid: joi.string().required(),

    });

    return Schema.validateAsync(userid);
};